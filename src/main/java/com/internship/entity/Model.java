package com.internship.entity;

/**
 * @author Velichko Spasov
 */

public class Model {
    private int id;
    private String name;

    public Model() {}

    public Model(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int getID() { return id; }
    public void setID(int id) { this.id = id; }

    @Override
    public String toString() {
        return "ID: " + id + " name: " + name;
    }
}
