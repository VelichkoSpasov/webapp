package com.internship.entity;

/**
 * @author Velichko Spasov
 */

public class Laptop {
    private int id;
    private int brand_id;
    private int model_id;
    private String price;

    public Laptop() {}

    public Laptop(int brand_id, int model_id, String price) {
        this.brand_id = brand_id;
        this.model_id = model_id;
        this.price = price;
    }

    public int getBrand_id() {
        return brand_id;
    }
    public void setBrand_id(int brand_id) {
        this.brand_id = brand_id;
    }

    public int getModel_id() {
        return model_id;
    }
    public void setModel_id(int model_id) {
        this.model_id = model_id;
    }

    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    public int getID() {
        return id;
    }
    public void setID(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ID: " + id + " brand_ID: " + brand_id + " model_ID: " + model_id + " price: " + price;
    }
}
