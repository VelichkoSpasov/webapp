package com.internship.entity;

/**
 * @author Velichko Spasov
 */

public class Brand {
    private int id;
    private String name;

    public Brand() {}
    public Brand(String name) {
        this.name = name;
    }

    public int getID() { return id; }
    public void setID(int id) { this.id = id; }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ID: " + id + " name: " + name;
    }
}
