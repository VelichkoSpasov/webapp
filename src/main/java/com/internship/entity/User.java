package com.internship.entity;

/**
 * @author Velichko Spasov
 */

public class User {
    private int id;
    private String firstName;
    private String middleName;
    private String lastName;
    private String UCN;
    private int laptopID;

    public User() {}

    public User(String firstName, String middleName, String lastName, String UCN, int laptopID) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.UCN = UCN;
        this.laptopID = laptopID;
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUCN() {
        return UCN;
    }
    public void setUCN(String UCN) {
        this.UCN = UCN;
    }

    public int getLaptopID() {
        return laptopID;
    }
    public void setLaptopID(int laptopID) {
        this.laptopID = laptopID;
    }

    public int getID() { return id; }
    public void setID(int id) { this.id = id; }

    @Override
    public String toString() {
        return "ID: " + id +
                " first name: " + firstName +
                " middle name: " + middleName +
                " last name: " + lastName +
                " UCN: " + UCN +
                " laptopID: " + laptopID;
    }
}
