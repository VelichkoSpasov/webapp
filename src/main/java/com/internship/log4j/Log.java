package com.internship.log4j;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * @author Velichko Spasov
 */

public class Log {
    /**
     * @param level - Level of log.
     * @param className - Name of class that is logging from.
     * @param message - Message to log.
     */
    public static void write(Level level, String className, String message) {
        Logger log = Logger.getLogger(className);
        log.log(level, message);
    }
}
