package com.internship.dao;

import com.internship.dao.jdbconnection.JDBConnection;
import com.internship.entity.Brand;
import com.internship.log4j.Log;
import org.apache.log4j.Level;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Velichko Spasov
 */

public class BrandDao implements DAO<Brand> {
    //private Logger log = Logger.getLogger(getClass().getName());
    private List<Brand> brands = new ArrayList<>();

    @Override
    public Optional get(long id) {
        return Optional.empty();
    }

    /**
     * Connecting and executing SELECT query.
     * @return - list of objects.
     */
    @Override
    public List selectAll() {
        String SQL_STATEMENT = "SELECT * FROM \"Computers\".brands";

        try {
            Connection conn = new JDBConnection().getInstance().connect();

            if (conn != null) {
                PreparedStatement prepStatement = conn.prepareStatement(SQL_STATEMENT);
                ResultSet rs = prepStatement.executeQuery();

                while (rs.next()) {
                    Brand brand = new Brand();
                    brand.setID(rs.getInt("id"));
                    brand.setName(rs.getString("name"));
                    brands.add(brand);
                }
            }
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
            Log.write(Level.ERROR, getClass().getName(),"SQL State: " + e.getSQLState() + "\n" + e.getMessage());

            return null;
        }

        for(Brand brand : brands) {
            System.out.println(brand.toString());
        }

        return brands;
    }

    /**
     * Connecting to database and execute insert query.
     * @param brand - object which will be inserted.
     */
    @Override
    public void insert(Brand brand) {
        String SQL_STATEMENT = "INSERT into \"Computers\".brands(name) VALUES('" + brand.getName() + "')";

        try {
            Connection conn = new JDBConnection().getInstance().connect();

            if (conn != null) {
                PreparedStatement prepStatement = conn.prepareStatement(SQL_STATEMENT);
                prepStatement.executeQuery();
            }
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
            Log.write(Level.ERROR, getClass().getName(), "SQL State: " + e.getSQLState() + "\n" + e.getMessage());
        }
    }

    /**
     * Connecting to database and execute update query
     * @param brand - object with new data.
     * @param id - id of object which will be updated.
     */
    @Override
    public void update(Brand brand, int id) {
        String SQL_STATEMENT = "UPDATE \"Computers\".brands SET name = '" + brand.getName() + "' WHERE id = " + id;

        try {
            Connection conn = new JDBConnection().getInstance().connect();

            if (conn != null) {
                PreparedStatement prepStatement = conn.prepareStatement(SQL_STATEMENT);
                prepStatement.executeQuery();
            }
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
            Log.write(Level.ERROR, getClass().getName(), "SQL State: " + e.getSQLState() + "\n" + e.getMessage());
        }
    }

    /*@Override
    public void delete(Brand brand) {
        String SQL_STATEMENT = "DELETE FROM \"Computers\".brands WHERE name = '" + brand.getName() + "'";

        try {
            Connection conn = new JDBConnection().getInstance().connect();

            if (conn != null) {
                PreparedStatement prepStatement = conn.prepareStatement(SQL_STATEMENT);
                prepStatement.executeQuery();
            }
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
            Log.write(Level.ERROR, getClass().getName(), "SQL State: " + e.getSQLState() + "\n" + e.getMessage());
        }
    }*/

    /**
     * Delete a tuple by id.
     * @param id - id of object which will be deleted.
     */
    @Override
    public void delete(int id) {
        String SQL_STATEMENT = "DELETE FROM \"Computers\".brands WHERE id = " + id;

        try {
            Connection conn = new JDBConnection().getInstance().connect();

            if (conn != null) {
                PreparedStatement prepStatement = conn.prepareStatement(SQL_STATEMENT);
                prepStatement.executeQuery();
            }
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
            Log.write(Level.ERROR, getClass().getName(), "SQL State: " + e.getSQLState() + "\n" + e.getMessage());
        }
    }
}
