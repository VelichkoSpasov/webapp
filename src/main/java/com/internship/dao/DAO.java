package com.internship.dao;

import java.util.List;
import java.util.Optional;

/**
 * @author Velichko Spasov
 */

public interface DAO<T> {
    Optional<T> get(long id);

    List<T> selectAll();

    void insert(T t);

    void update(T t1, int id);

    //void delete(T t);

    void delete(int id);
}
