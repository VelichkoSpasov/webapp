package com.internship.dao;

import com.internship.dao.jdbconnection.JDBConnection;
import com.internship.entity.Laptop;
import com.internship.log4j.Log;
import org.apache.log4j.Level;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Velichko Spasov
 */

public class LaptopDao implements DAO<Laptop> {
    private List<Laptop> laptops = new ArrayList<>();

    @Override
    public Optional<Laptop> get(long id) {
        return Optional.ofNullable(laptops.get((int) id));
    }

    /**
     * Connecting and executing SELECT query.
     * @return - list of objects.
     */
    @Override
    public List<Laptop> selectAll() {
        String SQL_STATEMENT = "SELECT * FROM \"Computers\".laptops";

        try {
            Connection conn = new JDBConnection().getInstance().connect();

            if (conn != null) {
                PreparedStatement prepStatement = conn.prepareStatement(SQL_STATEMENT);
                ResultSet rs = prepStatement.executeQuery();

                while (rs.next()) {
                    Laptop laptop = new Laptop();
                    laptop.setID(rs.getInt("id"));
                    laptop.setModel_id(rs.getInt("model_id"));
                    laptop.setBrand_id(rs.getInt("brand_id"));
                    laptop.setPrice(rs.getString("price"));
                    laptops.add(laptop);
                }
            }
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
            Log.write(Level.ERROR, getClass().getName(),"SQL State: " + e.getSQLState() + "\n" + e.getMessage());
        }

        return laptops;
    }

    /**
     * Connecting to database and execute insert query.
     * @param laptop - object which will be inserted.
     */
    @Override
    public void insert(Laptop laptop) {
        String SQL_STATEMENT = "INSERT into \"Computers\".laptops(model_id, brand_id, price) VALUES('" +
                laptop.getModel_id() + "', '" +
                laptop.getBrand_id() + "', '" +
                laptop.getPrice() + "')";

        try {
            Connection conn = new JDBConnection().getInstance().connect();

            if (conn != null) {
                PreparedStatement prepStatement = conn.prepareStatement(SQL_STATEMENT);
                prepStatement.executeQuery();
            }
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
            Log.write(Level.ERROR, getClass().getName(), "SQL State: " + e.getSQLState() + "\n" + e.getMessage());
        }
    }

    /**
     * Connecting to database and execute update query
     * @param laptop - object with new data.
     * @param id - id of object which will be updated.
     */
    @Override
    public void update(Laptop laptop, int id) {
        String SQL_STATEMENT = "UPDATE \"Computers\".laptops SET model_id = " + laptop.getModel_id()
                + ", brand_id = " + laptop.getBrand_id()
                + ", price = " + laptop.getPrice() + " WHERE id = " + id;

        try {
            Connection conn = new JDBConnection().getInstance().connect();

            if (conn != null) {
                PreparedStatement prepStatement = conn.prepareStatement(SQL_STATEMENT);
                prepStatement.executeQuery();
            }
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
            Log.write(Level.ERROR, getClass().getName(), "SQL State: " + e.getSQLState() + "\n" + e.getMessage());
        }
    }

    /*@Override
    public void delete(Laptop laptop) {

    }*/

    /**
     * Delete a tuple by id.
     * @param id - id of object which will be deleted.
     */
    @Override
    public void delete(int id) {
        String SQL_STATEMENT = "DELETE FROM \"Computers\".laptops WHERE id = " + id;

        try {
            Connection conn = new JDBConnection().getInstance().connect();

            if (conn != null) {
                PreparedStatement prepStatement = conn.prepareStatement(SQL_STATEMENT);
                prepStatement.executeQuery();
            }
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
            Log.write(Level.ERROR, getClass().getName(), "SQL State: " + e.getSQLState() + "\n" + e.getMessage());
        }
    }
}
