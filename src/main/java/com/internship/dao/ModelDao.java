package com.internship.dao;

import com.internship.dao.jdbconnection.JDBConnection;
import com.internship.entity.Model;
import com.internship.log4j.Log;
import org.apache.log4j.Level;
import java.sql.*;
import java.util.*;

/**
 * @author Velichko Spasov
 */

public class ModelDao implements DAO<Model> {
    //private Logger log = Logger.getLogger(getClass().getName());
    private List<Model> models = new ArrayList<>();

    @Override
    public Optional<Model> get(long id) {
        return Optional.ofNullable(models.get((int) id));
    }

    /**
     * Connecting and executing SELECT query.
     * @return - list of objects.
     */
    @Override
    public List<Model> selectAll() {
        String SQL_STATEMENT = "SELECT * FROM \"Computers\".models";

        try {
            Connection conn = new JDBConnection().getInstance().connect();

            if (conn != null) {
                PreparedStatement prepStatement = conn.prepareStatement(SQL_STATEMENT);
                ResultSet rs = prepStatement.executeQuery();

                while (rs.next()) {
                    Model model = new Model();
                    model.setID(rs.getInt("id"));
                    model.setName(rs.getString("name"));
                    models.add(model);
                }
            }
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
            Log.write(Level.ERROR, getClass().getName(),"SQL State: " + e.getSQLState() + "\n" + e.getMessage());
        }

        return models;
    }

    /**
     * Connecting to database and execute insert query.
     * @param model - object which will be inserted.
     */
    @Override
    public void insert(Model model) {
        String SQL_STATEMENT = "INSERT into \"Computers\".models(name) VALUES('" + model.getName() + "')";

        try {
            Connection conn = new JDBConnection().getInstance().connect();

            if (conn != null) {
                PreparedStatement prepStatement = conn.prepareStatement(SQL_STATEMENT);
                prepStatement.executeQuery();
            }
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
            Log.write(Level.ERROR, getClass().getName(), "SQL State: " + e.getSQLState() + "\n" + e.getMessage());
        }
    }

    /**
     * Connecting to database and execute update query
     * @param model - object with new data.
     * @param id - id of object which will be updated.
     */
    @Override
    public void update(Model model, int id) {
        String SQL_STATEMENT = "UPDATE \"Computers\".models SET name = '" + model.getName() + "' WHERE id = " + id;

        try {
            Connection conn = new JDBConnection().getInstance().connect();

            if (conn != null) {
                PreparedStatement prepStatement = conn.prepareStatement(SQL_STATEMENT);
                prepStatement.executeQuery();
            }
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
            Log.write(Level.ERROR, getClass().getName(), "SQL State: " + e.getSQLState() + "\n" + e.getMessage());
        }
    }

    /*@Override
    public void delete(Model model) {
        String SQL_STATEMENT = "DELETE FROM \"Computers\".models WHERE name = '" + model.getName() + "'";

        try {
            Connection conn = new JDBConnection().getInstance().connect();

            if (conn != null) {
                PreparedStatement prepStatement = conn.prepareStatement(SQL_STATEMENT);
                prepStatement.executeQuery();
            }
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
            Log.write(Level.ERROR, getClass().getName(), "SQL State: " + e.getSQLState() + "\n" + e.getMessage());
        }
    }*/

    /**
     * Delete a tuple by id.
     * @param id - id of object which will be deleted.
     */
    @Override
    public void delete(int id) {
        String SQL_STATEMENT = "DELETE FROM \"Computers\".models WHERE id = " + id;

        try {
            Connection conn = new JDBConnection().getInstance().connect();

            if (conn != null) {
                PreparedStatement prepStatement = conn.prepareStatement(SQL_STATEMENT);
                prepStatement.executeQuery();
            }
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
            Log.write(Level.ERROR, getClass().getName(), "SQL State: " + e.getSQLState() + "\n" + e.getMessage());
        }
    }
}
