package com.internship.dao.jdbconnection;

import com.internship.log4j.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

/**
 * @author Velichko Spasov
 */

public class JDBConnection {
    private static final JDBConnection instance;
    private Logger log = Logger.getLogger(getClass().getName());

    static {
        instance = new JDBConnection();
    }

    public JDBConnection getInstance() { return instance; }

    /**
     * Connection to the database.
     * @return the connection established with the database.
     */
    public Connection connect() {
        String propertiesFilePath = "D:\\LocalRepositories\\webapp\\src\\main\\resources\\JDBCSettings.properties";

        try {
            FileReader fileReader = new FileReader(propertiesFilePath);
            Properties properties = new Properties();

            properties.load(fileReader);

            String url = properties.getProperty("db.conn.url");
            String username = properties.getProperty("db.username");
            String password = properties.getProperty("db.password");

            Connection conn = DriverManager.getConnection(url, username, password);

            if(conn != null) {
                return conn;
            }
        } catch (FileNotFoundException e) {
            Log.write(Level.ERROR, getClass().getName(), "\n" + e.getMessage());
            System.out.println("FNF");
        } catch (IOException e) {
            Log.write(Level.ERROR, getClass().getName(),"\n" + e.getMessage());
            System.out.println("IO");
        } catch (SQLException e) {
            Log.write(Level.ERROR, getClass().getName(), "SQL State: " + e.getSQLState() + "\n" + e.getMessage());
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        }

        return null;
    }
}
