package com.internship.dao;

import com.internship.dao.jdbconnection.JDBConnection;
import com.internship.entity.Model;
import com.internship.entity.User;
import com.internship.log4j.Log;
import org.apache.log4j.Level;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Velichko Spasov
 */

public class UserDao implements DAO<User>{
    private List<User> users = new ArrayList<>();

    @Override
    public Optional<User> get(long id) {
        return Optional.ofNullable(users.get((int) id));
    }

    /**
     * Connecting and executing SELECT query.
     * @return - list of objects.
     */
    @Override
    public List<User> selectAll() {
        String SQL_STATEMENT = "SELECT * FROM \"Computers\".users";

        try {
            Connection conn = new JDBConnection().getInstance().connect();

            if (conn != null) {
                PreparedStatement prepStatement = conn.prepareStatement(SQL_STATEMENT);
                ResultSet rs = prepStatement.executeQuery();

                while (rs.next()) {
                    User user = new User();
                    user.setID(rs.getInt("id"));
                    user.setFirstName(rs.getString("first_name"));
                    user.setMiddleName(rs.getString("middle_name"));
                    user.setLastName(rs.getString("last_name"));
                    user.setUCN(rs.getString("UCN"));
                    user.setLaptopID(Integer.parseInt(rs.getString("laptop_id")));

                    users.add(user);
                }
            }
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
            Log.write(Level.ERROR, getClass().getName(),"SQL State: " + e.getSQLState() + "\n" + e.getMessage());
        }

        return users;
    }

    /**
     * Connecting to database and execute insert query.
     * @param user - object which will be inserted.
     */
    @Override
    public void insert(User user) {
        String SQL_STATEMENT = "INSERT into \"Computers\".users(first_name, middle_name, last_name, ucn, laptop_id) " +
                "VALUES('" + user.getFirstName() + "', '" + user.getMiddleName() +  "', '" +
                user.getLastName() + "', '" + user.getUCN() + "', " + user.getLaptopID() + ")";

        try {
            Connection conn = new JDBConnection().getInstance().connect();

            if (conn != null) {
                PreparedStatement prepStatement = conn.prepareStatement(SQL_STATEMENT);
                prepStatement.executeQuery();
            }
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
            Log.write(Level.ERROR, getClass().getName(), "SQL State: " + e.getSQLState() + "\n" + e.getMessage());
        }
    }

    /**
     * Connecting to database and execute update query
     * @param user - object with new data.
     * @param id - id of object which will be updated.
     */
    @Override
    public void update(User user, int id) {
        String SQL_STATEMENT = "UPDATE \"Computers\".users SET first_name = '" + user.getFirstName() +
                "', middle_name = '" + user.getMiddleName() +
                "', last_name = '" + user.getLastName() +
                "', ucn = '" + user.getUCN() +
                "', laptop_id = " + user.getLaptopID() + " WHERE id = " + id;
        try {
            Connection conn = new JDBConnection().getInstance().connect();

            if (conn != null) {
                PreparedStatement prepStatement = conn.prepareStatement(SQL_STATEMENT);
                prepStatement.executeQuery();
            }
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
            Log.write(Level.ERROR, getClass().getName(), "SQL State: " + e.getSQLState() + "\n" + e.getMessage());
        }
    }

    /**
     * Delete a tuple by id.
     * @param id - id of object which will be deleted.
     */
    @Override
    public void delete(int id) {
        String SQL_STATEMENT = "DELETE FROM \"Computers\".users WHERE id = " + id;

        try {
            Connection conn = new JDBConnection().getInstance().connect();

            if (conn != null) {
                PreparedStatement prepStatement = conn.prepareStatement(SQL_STATEMENT);
                prepStatement.executeQuery();
            }
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
            Log.write(Level.ERROR, getClass().getName(), "SQL State: " + e.getSQLState() + "\n" + e.getMessage());
        }
    }

    /*@Override
    public void update(Model model1, int id) {
        String SQL_STATEMENT = "UPDATE \"Computers\".models SET name = '" + model1.getName() + "' WHERE id = " + id;

        try {
            Connection conn = new JDBConnection().getInstance().connect();

            if (conn != null) {
                PreparedStatement prepStatement = conn.prepareStatement(SQL_STATEMENT);
                prepStatement.executeQuery();
            }
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
            Log.write(Level.ERROR, getClass().getName(), "SQL State: " + e.getSQLState() + "\n" + e.getMessage());
        }
    }

    @Override
    public void delete(int id) {
        String SQL_STATEMENT = "DELETE FROM \"Computers\".models WHERE id = " + id;

        try {
            Connection conn = new JDBConnection().getInstance().connect();

            if (conn != null) {
                PreparedStatement prepStatement = conn.prepareStatement(SQL_STATEMENT);
                prepStatement.executeQuery();
            }
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
            Log.write(Level.ERROR, getClass().getName(), "SQL State: " + e.getSQLState() + "\n" + e.getMessage());
        }
    }*/
}
