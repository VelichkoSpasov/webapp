package com.internship.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.internship.dao.BrandDao;
import com.internship.entity.Brand;
import com.internship.log4j.Log;
import org.apache.log4j.Level;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Velichko Spasov
 */

public class BrandService {
    private final String SELECT_MESSAGE = "Selected: ";
    private final String INSERT_MESSAGE = "Inserted: ";
    private final String UPDATE_MESSAGE = "Updated brand with id: ";
    private final String DELETE_MESSAGE = "Deleted brand with id: ";
    private final String INSERT_NAME = "name";
    private final String UPDATE_NAME = "name";
    private final String DELETE_NAME = "name";

    /**
     * Method which calls selectAll() from DAO and prints the result from the query.
     * @param response - data returned to the user.
     * @throws IOException
     */
    public void selectAll(HttpServletResponse response) throws IOException {
        PrintWriter pw = response.getWriter();
        ObjectMapper objMapper = new ObjectMapper();
        String brands = objMapper.writeValueAsString(new BrandDao().selectAll());
        pw.println(brands);
        Log.write(Level.INFO, getClass().getName(), SELECT_MESSAGE + brands);
    }

    /**
     * Calls DAO's insert method and inserts object with the requested data.
     * @param request - requested data from the user.
     */
    public void insertBrand(HttpServletRequest request) {
        Brand brand = new Brand();
        brand.setName(request.getParameter(INSERT_NAME));
        new BrandDao().insert(brand);
        Log.write(Level.INFO, getClass().getName(), INSERT_MESSAGE + brand.getName());
    }

    /**
     * Calls DAO's update method and update object by ID with the requested data.
     * @param request - requested object data and ID for update.
     */
    public void updateBrand(HttpServletRequest request) {
        Brand brand = new Brand();
        brand.setName(request.getParameter(UPDATE_NAME));
        new BrandDao().update(brand, Integer.parseInt(request.getParameter("id")));
        Log.write(Level.INFO, getClass().getName(), UPDATE_MESSAGE +
                request.getParameter("id") + " new brand name: " + brand.getName());
    }

    /*public void deleteBrand(HttpServletRequest request) {
        Brand brand = new Brand();
        brand.setName(request.getParameter(DELETE_NAME));
        new BrandDao().delete(brand);
        Log.write(Level.INFO, getClass().getName(), DELETE_MESSAGE + brand.getName());
    }*/

    /**
     * Calls DAO's delete method to delete object by ID.
     * @param request - requested ID to delete.
     */
    public void deleteBrand(HttpServletRequest request) {
        new BrandDao().delete(Integer.parseInt(request.getParameter("id")));
        Log.write(Level.INFO, getClass().getName(), DELETE_MESSAGE + request.getParameter("id"));
    }
}
