package com.internship.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.internship.dao.UserDao;
import com.internship.entity.User;
import com.internship.log4j.Log;
import org.apache.log4j.Level;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Velichko Spasov
 */

public class UserService {
    private final String SELECT_MESSAGE = "Selected: ";
    private final String INSERT_MESSAGE = "Inserted: ";
    private final String UPDATE_MESSAGE = "Updated user with id: ";
    private final String DELETE_MESSAGE = "Deleted user with id: ";
    private final String INSERT_FIRST_NAME = "fn";
    private final String INSERT_MIDDLE_NAME = "mn";
    private final String INSERT_LAST_NAME = "ln";
    private final String INSERT_UCN = "ucn";
    private final String INSERT_LAPTOPID = "pcid";
    private final String DELETE_NAME = "name";

    /**
     * Method which calls selectAll() from DAO and prints the result from the query.
     * @param response - data returned to the user.
     * @throws IOException
     */
    public void selectAll(HttpServletResponse response) throws IOException {
        PrintWriter pw = response.getWriter();
        ObjectMapper objMapper = new ObjectMapper();
        String users = objMapper.writeValueAsString(new UserDao().selectAll());
        pw.println(users);
        Log.write(Level.INFO, getClass().getName(), SELECT_MESSAGE + users);
    }

    /**
     * Calls DAO's insert method and inserts object with the requested data.
     * @param request - requested data from the user.
     */
    public void insertUser(HttpServletRequest request) {
        User user = new User();
        user.setFirstName(request.getParameter(INSERT_FIRST_NAME));
        user.setMiddleName(request.getParameter(INSERT_MIDDLE_NAME));
        user.setLastName(request.getParameter(INSERT_LAST_NAME));
        user.setUCN(request.getParameter(INSERT_UCN));
        user.setLaptopID(Integer.parseInt(request.getParameter(INSERT_LAPTOPID)));
        new UserDao().insert(user);
        Log.write(Level.INFO, getClass().getName(), INSERT_MESSAGE +
                " first name: " + user.getFirstName() +
                " middle name: " + user.getMiddleName() +
                " last name: " + user.getLastName() +
                " UCN: " + user.getUCN() +
                " laptopID: " + user.getLaptopID());
    }

    /**
     * Calls DAO's update method and update object by ID with the requested data.
     * @param request - requested object data and ID for update.
     */
    public void updateUser(HttpServletRequest request) {
        User user = new User();
        user.setFirstName(request.getParameter("fn"));
        user.setMiddleName(request.getParameter("mn"));
        user.setLastName(request.getParameter("ln"));
        user.setUCN(request.getParameter("ucn"));
        user.setLaptopID(Integer.parseInt(request.getParameter("pcid")));

        new UserDao().update(user, Integer.parseInt(request.getParameter("id")));
        Log.write(Level.INFO, getClass().getName(), UPDATE_MESSAGE
                + request.getParameter("id") + " is changed to: " + user.getFirstName());
    }

    /**
     * Calls DAO's delete method to delete object by ID.
     * @param request - requested ID to delete.
     */
    public void deleteUser(HttpServletRequest request) {
        new UserDao().delete(Integer.parseInt(request.getParameter("id")));
        Log.write(Level.INFO, getClass().getName(), DELETE_MESSAGE + request.getParameter("id"));
    }
}
