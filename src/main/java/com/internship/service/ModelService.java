package com.internship.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.internship.dao.ModelDao;
import com.internship.entity.Model;
import com.internship.log4j.Log;
import org.apache.log4j.Level;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Velichko Spasov
 */

public class ModelService{
    private final String SELECT_MESSAGE = "Selected: ";
    private final String INSERT_MESSAGE = "Inserted: ";
    private final String UPDATE_MESSAGE = "Updated model with id: ";
    private final String DELETE_MESSAGE = "Deleted model with id: ";
    private final String INSERT_NAME = "name";
    private final String UPDATE_NAME = "name";
    private final String DELETE_NAME = "name";

    /**
     * Method which calls selectAll() from DAO and prints the result from the query.
     * @param response - data returned to the user.
     * @throws IOException
     */
    public void selectAll(HttpServletResponse response) throws IOException {
        PrintWriter pw = response.getWriter();
        ObjectMapper objMapper = new ObjectMapper();
        String models = objMapper.writeValueAsString(new ModelDao().selectAll());
        pw.println(models);
        Log.write(Level.INFO, getClass().getName(), SELECT_MESSAGE + models);
    }

    /**
     * Calls DAO's insert method and inserts object with the requested data.
     * @param request - requested data from the user.
     */
    public void insertModel(HttpServletRequest request) {
        Model model = new Model();
        model.setName(request.getParameter(INSERT_NAME));
        new ModelDao().insert(model);
        Log.write(Level.INFO, getClass().getName(), INSERT_MESSAGE + model.getName());
    }

    /**
     * Calls DAO's update method and update object by ID with the requested data.
     * @param request - requested object data and ID for update.
     */
    public void updateModel(HttpServletRequest request) {
        Model model1 = new Model();
        model1.setName(request.getParameter(UPDATE_NAME));
        new ModelDao().update(model1, Integer.parseInt(request.getParameter("id")));
        Log.write(Level.INFO, getClass().getName(), UPDATE_MESSAGE
                + request.getParameter("id") + " is changed to: " + model1.getName());
    }

    /*public void deleteModel(HttpServletRequest request) {
        Model model = new Model();
        model.setName(request.getParameter(DELETE_NAME));
        new ModelDao().delete(model);
        Log.write(Level.INFO, getClass().getName(), DELETE_MESSAGE + model.getName());
    }*/

    /**
     * Calls DAO's delete method to delete object by ID.
     * @param request - requested ID to delete.
     */
    public void deleteModel(HttpServletRequest request) {
        new ModelDao().delete(Integer.parseInt(request.getParameter("id")));
        Log.write(Level.INFO, getClass().getName(), DELETE_MESSAGE + request.getParameter("id"));
    }
}
