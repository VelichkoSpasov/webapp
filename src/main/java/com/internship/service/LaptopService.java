package com.internship.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.internship.dao.LaptopDao;
import com.internship.entity.Laptop;
import com.internship.log4j.Log;
import org.apache.log4j.Level;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Velichko Spasov
 */

public class LaptopService {
    private final String SELECT_MESSAGE = "Selected: ";
    private final String INSERT_MESSAGE = "Inserted: ";
    private final String UPDATE_MESSAGE = "Updated: ";
    private final String DELETE_MESSAGE = "Deleted: ";
    private final String BRAND_ID = "brand_id";
    private final String MODEL_ID = "model_id";
    private final String PRICE = "price";

    /**
     * Method which calls selectAll() from DAO and prints the result from the query.
     * @param response - data returned to the user.
     * @throws IOException
     */
    public void selectAll(HttpServletResponse response) throws IOException {
        PrintWriter pw = response.getWriter();
        ObjectMapper objMapper = new ObjectMapper();
        String laptops = objMapper.writeValueAsString(new LaptopDao().selectAll());
        pw.println(laptops);
        Log.write(Level.INFO, getClass().getName(), SELECT_MESSAGE + laptops);
    }

    /**
     * Calls DAO's insert method and inserts object with the requested data.
     * @param request - requested data from the user.
     */
    public void insertLaptop(HttpServletRequest request) {
        Laptop laptop = new Laptop();
        laptop.setBrand_id(Integer.parseInt(request.getParameter(BRAND_ID)));
        laptop.setModel_id(Integer.parseInt(request.getParameter(MODEL_ID)));
        laptop.setPrice(request.getParameter(PRICE));
        new LaptopDao().insert(laptop);
        Log.write(Level.INFO, getClass().getName(), INSERT_MESSAGE +
                laptop.getBrand_id() + " " +
                laptop.getModel_id() + " " +
                laptop.getPrice());
    }

    /**
     * Calls DAO's update method and update object by ID with the requested data.
     * @param request - requested object data and ID for update.
     */
    public void updateLaptop(HttpServletRequest request) {
        Laptop laptop1 = new Laptop();
        laptop1.setModel_id(Integer.parseInt(request.getParameter(MODEL_ID)));
        laptop1.setBrand_id(Integer.parseInt(request.getParameter(BRAND_ID)));
        laptop1.setPrice(request.getParameter(PRICE));
        new LaptopDao().update(laptop1, Integer.parseInt(request.getParameter("id")));
        Log.write(Level.INFO, getClass().getName(),
                UPDATE_MESSAGE + "Laptop with ID: " + request.getParameter("id"));
    }

    /**
     * Calls DAO's delete method to delete object by ID.
     * @param request - requested ID to delete.
     */
    public void deleteLaptop(HttpServletRequest request) {
        new LaptopDao().delete(Integer.parseInt(request.getParameter("id")));
        Log.write(Level.INFO, getClass().getName(), DELETE_MESSAGE + "Laptop with id: " + request.getParameter("id"));
    }
}
