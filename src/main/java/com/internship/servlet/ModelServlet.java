package com.internship.servlet;

import com.internship.log4j.Log;
import com.internship.service.ModelService;
import org.apache.log4j.Level;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 * @author Velichko Spasov
 */

public class ModelServlet extends HttpServlet {
    public void init() {}

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("text/html");

        String title = "Using GET Method to Read Form Data";
        String docType = "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";

        /*pw.println("<h1>" + message + "</h1>");
        pw.println(docType +
                "<html>\n" +
                "<head><title>" + title + "</title></head>\n" +
                "<body bgcolor = \"#f0f0f0\">\n" +
                "<h1 align = \"center\">" + title + "</h1>\n" +
                "<ul>\n" +
                "  <li><b>First Name</b>: "
                + request.getParameter("id") + "\n" +
                "</ul>\n" +
                "</body>" +
                "</html>"
        );*/
        try {
            new ModelService().selectAll(response);
        }
        catch (IOException e) {
            System.out.println(getClass().getName() + "IOException.");
            Log.write(Level.ERROR, getClass().getName(), "IOException: " + e.getMessage());
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        new ModelService().insertModel(request);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) {
        new ModelService().updateModel(request);
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) {
        new ModelService().deleteModel(request);
    }
}

