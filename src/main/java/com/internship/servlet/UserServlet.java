package com.internship.servlet;

import com.internship.log4j.Log;
import com.internship.service.UserService;
import org.apache.log4j.Level;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Velichko Spasov
 */

public class UserServlet extends HttpServlet {
    public void init() {}

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            new UserService().selectAll(response);
        }
        catch (IOException e) {
            System.out.println(getClass().getName() + "IOException.");
            Log.write(Level.ERROR, getClass().getName(), "IOException: " + e.getMessage());
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        new UserService().insertUser(request);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) {
        new UserService().updateUser(request);
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) {
        new UserService().deleteUser(request);
    }
}
