package com.internship.servlet;

import com.internship.log4j.Log;
import com.internship.service.LaptopService;
import org.apache.log4j.Level;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Velichko Spasov
 */

public class LaptopServlet extends HttpServlet {
    public void init() {}

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("text/html");

        try {
            new LaptopService().selectAll(response);
        }
        catch (IOException e) {
            System.out.println(getClass().getName() + "IOException.");
            Log.write(Level.ERROR, getClass().getName(), "IOException: " + e.getMessage());
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        new LaptopService().insertLaptop(request);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) {
        new LaptopService().updateLaptop(request);
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) {
        new LaptopService().deleteLaptop(request);
    }
}
